# Plugin de communication pour Garradin

Ce plugin est développé pour l'application de gestion associative Garradin
(voir https://garradin.eu).

La vocation de ce plugin est d'aider à la gestion de la communication d'une association.

La gestion de la communication avec ce plugin se traduit sur troi niveaux:

1. Un niveau stratégique :
   - les supports de communication à développer,
   - les segments qui définissent les différents publics ciblés,
   - les objectifs de l'association en matière de communication.
2. Un niveau tactique, où se définissent :
   - les produits à mettre en avant dans le cadre de la politique de communication (entités `Product`),
   - les campagnes qui visent à mettre en avant un produit aurpès d'un segment,
   - les différents formats de communication utilisés dans le cadre de ces campagnes.
3. Un niveau opérationnel, où se prévoit le calendrier des publications et leur contenu.

## Entités

| Entité      | Description                                                      | Niveau |
|-------------|------------------------------------------------------------------|--------|
| Support     | Support ou canal de communication                                |      1 |
| Segment     | Segment et public                                                |      1 |
| Objective   | Objectif de l'association en matière de communication            |      1 |
| Indicator   | Indicateur de réussite d'un objectif pour mesurer son avancement |      1 |
| Product     | Produit à promouvoir dans le cadre de la communication           |      2 |
| Campaign    | Campagne de communication liant un produit à son public          |      2 |
| Template    | Format type utilisé dans le cadre d'une campagne                 |      2 |
| Field       | Champ de texte utilisé pour composer un format                   |      2 |
| Publication | Publication programmée dans le cadre du plan de communication    |      3 |
| Text        | Contenu des différents champs d'une publication.                 |      3 |
