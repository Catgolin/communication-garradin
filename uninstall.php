<?php

namespace Garradin;

$db = DB::getInstance();
$db->begin();

$db->exec('DROP TABLE IF EXISTS com_template_supports');
$db->exec('DROP TABLE IF EXISTS com_campaign_objectives');

$db->exec('DROP TABLE IF EXISTS com_text');
$db->exec('DROP TABLE IF EXISTS com_publication');
$db->exec('DROP TABLE IF EXISTS com_field');
$db->exec('DROP TABLE IF EXISTS com_template');
$db->exec('DROP TABLE IF EXISTS com_campaign');
$db->exec('DROP TABLE IF EXISTS com_indicator');
$db->exec('DROP TABLE IF EXISTS com_objective');
$db->exec('DROP TABLE IF EXISTS com_product');
$db->exec('DROP TABLE IF EXISTS com_segment');
$db->exec('DROP TABLE IF EXISTS com_support');

$db->exec('DROP TABLE IF EXISTS com_format_supports');
$db->exec('DROP TABLE IF EXISTS com_format');

$db->commit();
