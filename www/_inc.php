<?php

require_once __DIR__ . '/../lib/Entities/Objective.php';
require_once __DIR__ . '/../lib/Entities/Indicator.php';
require_once __DIR__ . '/../lib/Entities/Product.php';
require_once __DIR__ . '/../lib/Entities/Segment.php';
require_once __DIR__ . '/../lib/Entities/Campaign.php';
require_once __DIR__ . '/../lib/Entities/Field.php';
require_once __DIR__ . '/../lib/Entities/Support.php';
require_once __DIR__ . '/../lib/Entities/Publication.php';
require_once __DIR__ . '/../lib/Entities/Template.php';
require_once __DIR__ . '/../lib/Entities/Text.php';
require_once __DIR__ . '/../lib/Pages.php';
