<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Product;
use Garradin\Entities\Services\Service;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Product::class);
$pages->edit(
    [],
    [],
    function($product) use ($session) {
        if((bool) f('create_service')) {
            if($session->canAccess($session::SECTION_USERS, $session::ACCESS_ADMIN) && $product->service === null) {
                $product->createService();
            }
        }
        if(qg('related')) {
            $product->setRelated((int) qg('related'));
        }
    },
    null,
    function() use($pages, $tpl) {
        $tpl->assign('types', Product::TYPES);
        $em = EntityManager::getInstance(Service::class);
        $services = $em->all('SELECT * FROM @TABLE WHERE end_date IS NULL OR end_date < CURRENT_TIMESTAMP');
        $array = ['' => '-- Aucun service --'];
        foreach($services as $service) {
            $array[$service->id] = $service->label;
        }
        $tpl->assign('services', $array);
    }
);
