<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Product;
use Garradin\Communication\Pages;
use KD2\DB\EntityManager;

$pages = new Pages(Product::class);
$pages->delete(function($product) {
    if(f('confirm_delete')) {
        $em = EntityManager::getInstance(Product::class);
        $related = $em->all('SELECT * FROM @TABLE WHERE id_related=?', $product->id);
        foreach($related as $delete) {
            $delete->delete();
        }
    }
});
