<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Product;
use Garradin\Entities\Communication\Campaign;
use Garradin\Entities\Services\Service;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Product::class);
$pages->display(function($product) use($pages, $tpl) {
    $pages->setEntity('service', $product->id_service, Service::class);

    $tpl->assign('type', Product::TYPES[$product->type]['label']);

    $em = EntityManager::getInstance(Product::class);
    $children = $em->all('SELECT * FROM @TABLE WHERE id_related=? ORDER BY deadline, end, start, date_event, id', $product->id);
    $tpl->assign('children', $children);

    $em = EntityManager::getInstance(Campaign::class);
    $campaigns = $em->all('SELECT * FROM @TABLE WHERE id_product=?', $product->id);
    $tpl->assign('campaigns', $campaigns);
});
