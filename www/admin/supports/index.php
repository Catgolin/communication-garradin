<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Support;
use KD2\DB\EntityManager;

$em = EntityManager::getInstance(Support::class);
$supports = $em->all('SELECT * FROM @TABLE ORDER BY id');

$tpl->assign('supports', $supports);

$tpl->display(PLUGIN_ROOT . '/templates/supports/index.tpl');
