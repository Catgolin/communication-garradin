<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Support;
use Garradin\Entities\Communication\Segment;
use Garradin\Entities\Communication\Product;
use Garradin\Entities\Communication\Campaign;
use KD2\DB\EntityManager;

if(empty(EntityManager::getInstance(Support::class)
         ->all('SELECT * FROM @TABLE'))) {
    Utils::redirect(PLUGIN_URL . 'index.php');
}

if(empty(EntityManager::getInstance(Segment::class)
         ->all('SELECT * FROM @TABLE'))) {
    Utils::redirect(PLUGIN_URL . 'index.php');
}

$em = EntityManager::getInstance(Product::class);
$products = $em->all('SELECT * FROM @TABLE WHERE id_related IS NULL');
$tpl->assign('products', $products);


$em = EntityManager::getInstance(Campaign::class);
$campaigns = $em->all('SELECT * FROM @TABLE WHERE end IS NULL OR end >= CURRENT_TIMESTAMP ORDER BY start, id');
$tpl->assign('campaigns', $campaigns);

$tpl->display(PLUGIN_ROOT . '/templates/campaigns.tpl');
