<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Publication;
use Garradin\Entities\Communication\Support;
use Garradin\Entities\Communication\Segment;
use Garradin\Entities\Communication\Product;
use Garradin\Entities\Communication\Campaign;
use KD2\DB\EntityManager;

if(empty(EntityManager::getInstance(Support::class)
         ->all('SELECT * FROM @TABLE'))) {
    Utils::redirect(PLUGIN_URL . 'index.php');
}

if(empty(EntityManager::getInstance(Segment::class)
         ->all('SELECT * FROM @TABLE'))) {
    Utils::redirect(PLUGIN_URL . 'index.php');
}

if(empty(EntityManager::getInstance(Product::class)
         ->all('SELECT * FROM @TABLE'))) {
    Utils::redirect(PLUGIN_URL . 'campaigns.php');
}

if(empty(EntityManager::getInstance(Campaign::class)
         ->all('SELECT * FROM @TABLE'))) {
    Utils::redirect(PLUGIN_URL . 'campaigns.php');
}

$em = EntityManager::getInstance(Publication::class);
$publications = $em->all('SELECT * FROM @TABLE ORDER BY release_date');
$tpl->assign('publications', $publications);

$tpl->display(PLUGIN_ROOT . '/templates/calendar.tpl');
