<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Field;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Field::class);
$pages->delete();
