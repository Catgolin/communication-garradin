<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Field;
use Garradin\Entities\Communication\Template;
use Garradin\Entities\Communication\Support;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Field::class);
$pages->edit(
    [],
    [
        'template' => Template::class,
        'support' => Support::class,
    ],
    null,
    function($field) {
        if($field->id_support) {
            Utils::redirect(PLUGIN_URL . 'supports/display.php?id=' . $field->id_support);
        }
        if($field->id_template) {
            Utils::redirect(PLUGIN_URL . 'templates/display.php?id=' . $field->id_template);
        }
    }
);
