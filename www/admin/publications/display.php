<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Product;
use Garradin\Entities\Communication\Publication;
use Garradin\Entities\Communication\Campaign;
use Garradin\Entities\Communication\Template;
use Garradin\Entities\Communication\Text;
use Garradin\Entities\Communication\Field;
use Garradin\Entities\Services\Service;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Publication::class);
$pages->display(function($publication) use ($pages, $tpl) {
    $pages->setEntity('template', $publication->id_template, Template::class);
    $pages->setEntity('campaign', $publication->id_campaign, Campaign::class);
    $em = EntityManager::getInstance(Text::class);
    $texts = $em->all('SELECT * FROM @TABLE WHERE id_publication=?', $publication->id);
    $tpl->assign('texts', $texts);
});
