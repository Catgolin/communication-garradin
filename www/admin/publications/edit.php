<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Campaign;
use Garradin\Entities\Communication\Product;
use Garradin\Entities\Communication\Template;
use Garradin\Entities\Communication\Publication;
use Garradin\Entities\Communication\Text;
use Garradin\Entities\Communication\Field;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Publication::class);
$pages->edit(
    [
        'template' => Template::class,
        'campaign' => Campaign::class,
    ],
    [],
    null,
    function($publication) {
        foreach($publication->getFields() as $field) {
            $em = EntityManager::getInstance(Text::class);
            $text = $em->one('SELECT * FROM @TABLE WHERE id_field=? AND id_publication=?', $field->id, $publication->id);
            if(!$text) {
                $text = new Text();
                $text->id_field = $field->id;
                $text->id_publication = $publication->id;
            }
            $name = 'field_' . $field->id;
            $text->content = $_POST[$name];
            $text->save();
        }
    },
    function($publication) use($pages, $tpl) {
        $pages->setEntity('template', $publication->id_template, Template::class);
        $pages->setEntity('campaign', $publication->id_campaign, Campaign::class);
        $pages->setEntity('product', $publication->id_product, Product::class);
    }
);
