<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Product;
use KD2\DB\EntityManager;

$em = EntityManager::getInstance(Product::class);
$events = $em->all('SELECT * FROM @TABLE WHERE type=? AND date_event >= CURRENT_TIMESTAMP ORDER BY deadline, date_event', Product::TYPE_EVENT);
$durations = $em->all('SELECT * FROM @TABLE WHERE type=? AND end >= CURRENT_TIMESTAMP ORDER BY start', Product::TYPE_DURATION);
$other = $em->all('SELECT * FROM @TABLE WHERE type=? ORDER BY deadline', Product::TYPE_OTHER);
$past = $em->all('SELECT * FROM @TABLE WHERE type=? AND date_event < CURRENT_TIMESTAMP ORDER BY date_event DESC', Product::TYPE_EVENT);

$tpl->assign('events', $events);
$tpl->assign('durations', $durations);
$tpl->assign('products', $other);
$tpl->assign('past_events', $past);

$tpl->display(PLUGIN_ROOT . '/templates/products/index.tpl');
