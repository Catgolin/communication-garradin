<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Objective;
use Garradin\Entities\Communication\Indicator;
use Garradin\Communication\Pages;
use KD2\DB\EntityManager;

$pages = new Pages(Objective::class, $tpl);
$pages->display(function($objective) use($tpl) {
    $tpl->assign('indicators', $objective->getIndicators());
});
