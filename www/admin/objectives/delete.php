<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Objective;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Objective::class);
$pages->delete();
