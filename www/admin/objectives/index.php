<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Objective;
use Garradin\Entities\Communication\Indicator;
use Garradin\Entities\Communication\Segment;
use KD2\DB\EntityManager;

$em = EntityManager::getInstance(Objective::class);
$objectives = $em->all('SELECT * FROM @TABLE WHERE end IS NULL OR end >= CURRENT_TIMESTAMP ORDER BY id');

$indicators = [];
foreach($objectives as $obj) {
    $indicators[$obj->id] = $obj->getIndicators();
}

$tpl->assign('objectives', $objectives);
$tpl->assign('indicators', $indicators);

$segmentsEm = EntityManager::getInstance(Segment::class);
$segments = $segmentsEm->all('SELECT * FROM @TABLE WHERE title IS NOT NULL');
$tpl->assign('segments', $segments);

$tpl->display(PLUGIN_ROOT . '/templates/objectives/index.tpl');
