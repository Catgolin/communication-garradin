<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Segment;
use Garradin\Entities\Communication\Campaign;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Segment::class);
$pages->delete();
