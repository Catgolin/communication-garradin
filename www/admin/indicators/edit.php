<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Indicator;
use Garradin\Entities\Communication\Objective;
use Garradin\Communication\Pages;
use KD2\DB\EntityManager;

$pages = new Pages(Indicator::class, $tpl);

$pages->edit(
    [
        'objective' => Objective::class,
    ],
    [],
    null,
    function($indicator) {
        Utils::redirect(PLUGIN_URL . 'objectives/display.php?id=' . $indicator->id_objective);
    },
);

