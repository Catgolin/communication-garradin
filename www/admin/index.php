<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Support;
use Garradin\Entities\Communication\Segment;
use Garradin\Entities\Communication\Objective;
use Garradin\Entities\Communication\Indicator;
use KD2\DB\EntityManager;

$em = EntityManager::getInstance(Support::class);
$supports = $em->all('SELECT * FROM @TABLE');
$tpl->assign('supports', $supports);

$em = EntityManager::getInstance(Objective::class);
$objectives = $em->all('SELECT * FROM @TABLE WHERE end IS NULL OR end >= CURRENT_TIMESTAMP');
$tpl->assign('objectives', $objectives);

$em = EntityManager::getInstance(Segment::class);
$segments = $em->all('SELECT * FROM @TABLE');
$tpl->assign('segments', $segments);

$tpl->display(PLUGIN_ROOT . '/templates/index.tpl');
