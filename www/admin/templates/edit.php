<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Template;
use Garradin\Entities\Communication\Campaign;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Template::class);
$pages->edit(
    [],
    ['campaign' => Campaign::class]
);
