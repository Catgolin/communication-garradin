<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Template;
use Garradin\Entities\Communication\Campaign;
use Garradin\Entities\Communication\Field;
use Garradin\Entities\Communication\Publication;
use KD2\DB\EntityManager;
use Garradin\Communication\Pages;

$pages = new Pages(Template::class);
$pages->display(function($template) use ($pages, $tpl) {
    $pages->setEntity('campaign', $template->id_campaign, Campaign::class);

    $em = EntityManager::getInstance(Field::class);
    $fields = $em->all('SELECT * FROM @TABLE WHERE id_template=?', $template->id);
    $tpl->assign('fields', $fields);

    $em = EntityManager::getInstance(Publication::class);
    $publications = $em->all('SELECT * FROM @TABLE WHERE id_template=?', $template->id);
    $tpl->assign('publications', $publications);

    $createPublication = \Garradin\PLUGIN_URL . 'publications/edit.php?template=' . $template->id;
    if($template->id_campaign) {
        $createPublication .= '&campaign=' . $template->id_campaign;
    }
    $tpl->assign('link_create', $createPublication);
});
