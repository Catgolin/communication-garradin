<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Campaign;
use Garradin\Entities\Communication\Segment;
use Garradin\Entities\Communication\Product;
use Garradin\Entities\Communication\Indicator;
use KD2\DB\EntityManager;

$em = EntityManager::getInstance(Campaign::class);
$campaigns = $em->all('SELECT * FROM @TABLE WHERE end IS NULL OR end >= CURRENT_TIMESTAMP ORDER BY id');

$productEm = EntityManager::getInstance(Product::class);
$products = [];
foreach($campaigns as $campaign) {
    $products[] = $productEm->one('SELECT * FROM @TABLE WHERE id=?', $campaign->id_product)->title;
}

$targetEm = EntityManager::getInstance(Segment::class);
$targets = [];
foreach($campaigns as $campaign) {
    $targets[] = $targetEm->one('SELECT * FROM @TABLE WHERE id=?', $campaign->id_primary_target)->title;
}

$tpl->assign('campaigns', $campaigns);
$tpl->assign('products', $products);
$tpl->assign('targets', $targets);

$tpl->display(PLUGIN_ROOT . '/templates/campaigns/index.tpl');
