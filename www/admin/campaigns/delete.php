<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Entities\Communication\Campaign;
use Garradin\Communication\Pages;
use KD2\DB\EntityManager;

$pages = new Pages(Campaign::class, $tpl);
$pages->delete();
