<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use Garradin\Communication\Pages;
use Garradin\Entities\Communication\Campaign;
use Garradin\Entities\Communication\Segment;
use Garradin\Entities\Communication\Product;
use KD2\DB\EntityManager;

$pages = new Pages(Campaign::class, $tpl);
$pages->edit([], ['product' => Product::class, 'segment' => Segment::class], null, null, function($campaign) use ($pages, $tpl) {
    $pages->setEntitiesAsArray(Product::class, 'products');
    $pages->setEntitiesAsArray(Segment::class, 'segments');
});
