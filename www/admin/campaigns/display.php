<?php

namespace Garradin;

require_once PLUGIN_ROOT . '/www/_inc.php';

use garradin\Communication\Pages;
use Garradin\Entities\Communication\Campaign;
use Garradin\Entities\Communication\Segment;
use Garradin\Entities\Communication\Product;
use Garradin\Entities\Communication\Template;
use Garradin\Entities\Communication\Publication;
use KD2\DB\EntityManager;

$page = new Pages(Campaign::class, $tpl);
$page->display(function(Campaign $campaign) use ($page, $tpl) {
    $page->setEntity('primary_target', $campaign->id_primary_target, Segment::class);
    $page->setEntity('product', $campaign->id_product, Product::class);

    $em = EntityManager::getInstance(Template::class);
    $templates = $em->all('SELECT * FROM @TABLE WHERE id_campaign=?', $campaign->id);
    $tpl->assign('templates', $templates);

    $em = EntityManager::getInstance(Publication::class);
    $publications = $em->all('SELECT * FROM @TABLE WHERE id_campaign=?', $campaign->id);
    $tpl->assign('publications', $publications);
});
