<?php

namespace Garradin\Entities\Communication;

use Garradin\Entity;
use KD2\DB\EntityManager;

class Segment extends Entity
{
    const TABLE = 'com_segment';

    protected $id;
    protected $title;
    protected $description;

    protected $_types = [
        'id' => 'int',
        'title' => 'string',
        'description' => '?string',
    ];

    public static function create(string $title, ?string $description = null): Segment
    {
        $this->title = $title;
        $this->description = $description;
    }
}
