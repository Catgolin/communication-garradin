<?php

namespace Garradin\Entities\Communication;

use Garradin\Entity;
use KD2\DB\EntityManager;
use Garradin\Entities\Objective;

/**
 * Represents an indocator for the completion of an objective
 */
class Indicator extends Entity
{
    const TABLE = 'com_indicator';

    const NOT_NUMERIC = 0;
    const NOT_COMPLETE = 1;
    const COMPLETED = 2;
    const LATE = 3;

    protected $id;
    protected $id_objective;
    protected $title;
    protected $description;
    protected $metric; // Unit of measurment of the indicator
    protected $url; // Optionnal link for validation of the indicator
    protected $target; // Target value for the indicator
    protected $value; // Actual value of the indicator
    protected $deadline;

    protected $_types = [
        'id' => 'int',
        'title' => 'string',
        'description' => '?string',
        'id_objective' => 'int',
        'metric' => '?string',
        'url' => '?string',
        'target' => '?string',
        'value' => '?string',
        'deadline' => '?date',
    ];

    public static function create(string $title, Objective $objective, ?string $description = null, ?string $metric = null, ?string $target = null, ?string $value = null, ?DateTime $deadine = null): Indicator
    {
        $indicator = new self;
        $indicator->title = $title;
        $indicator->id_objective = $objective->id;
        $indicator->description = $description;
        $indicator->metric = $metric;
        $indicator->url = $url;
        $indicator->target = $target;
        $indicator->value = $value;
        $indicator->deadline = $deadline;
    }

    public function isNumeric(): bool
    {
        if(!is_numeric($this->target)) {
            return false;
        }
        return $this->value === null || is_numeric($this->value);
    }

    public function percentage(): ?float
    {
        if(!is_numeric($this->target) || !is_numeric($this->value)) {
            return null;
        }
        $target = (float) $this->target;
        $value = (float) $this->value;
        return ($value * 100) / $target;
    }

    public function getStatus(): int
    {
        if(!$this->isNumeric()) {
            return self::NOT_NUMERIC;
        }
        if($this->percentage() > 100) {
            return self::COMPLETED;
        }
        if($this->deadline !== null && $this->deadline <= new \DateTime()) {
            return self::LATE;
        }
        return self::NOT_COMPLETED;
    }

    public function asDetailsArray(): array
    {
        $status = null;
        switch($this->getStatus()) {
        case self::COMPLETED:
            $status = 'Validé';
            break;
        case self::NOT_COMPLETED:
            $status = 'En cours';
            break;
        case self::LATE:
            $status = 'Dépassé';
            break;
        }
        return [
            'Titre' => $this->title,
            'Description' => $this->description,
            'Valeur' => $this->value,
            'But' => $this->target,
            'Unité' => $this->metric,
            'Objectif' => (new Objective($this->id_objective))->title,
            'Date limite' => $this->deadline,
            'Statut' => $status,
        ];
    }
}
