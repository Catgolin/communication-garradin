<?php

namespace Garradin\Entities\Communication;

use Garradin\Entity;
use KD2\DB\EntityManager;
use Garradin\Entities\Services\Service;

class Product extends Entity
{
    const TABLE = 'com_product';

    const TYPE_EVENT = 1;
    const TYPE_DURATION = 2;
    const TYPE_OTHER = 3;
    const TYPES = [
        self::TYPE_EVENT => [
            'id' => self::TYPE_EVENT,
            'label' => 'Événement',
        ],
        self::TYPE_DURATION => [
            'id' => self::TYPE_DURATION,
            'label' => 'Période',
            'help' => 'Exposition, saison, ou tout autre événement avec un début et une fin',
        ],
        self::TYPE_OTHER => [
            'id' => self::TYPE_OTHER,
            'label' => 'Autre',
        ],
    ];

    protected $id;
    protected $title;
    protected $description;
    protected $action;
    protected $url;
    protected $deadline;
    protected $type = self::TYPE_OTHER;
    protected $start;
    protected $end;
    protected $date_event;
    protected $time_start;
    protected $time_end;
    protected $id_service;
    protected $id_related;

    protected $_types = [
        'id' => 'int',
        'title' => 'string',
        'description' => '?string',
        'action' => '?string',
        'url' => '?string',
        'deadline' => '?date',
        'type' => 'int',
        'start' => '?date',
        'end' => '?date',
        'date_event' => '?date',
        'time_start' => '?string',
        'time_end' => '?string',
        'id_service' => '?int',
        'id_related' => '?int',
    ];

    /**
     * @override
     */
    public function selfCheck(): void
    {
        switch($this->type) {
        case self::TYPE_EVENT:
            $this->assert($this->date_event instanceof \DateTime, 'La date de l\'évènement doit être précisée');
            break;
        case self::TYPE_DURATION:
            $this->assert($this->start instanceof \DateTime, 'La date de début doit être précisée');
            break;
        }
    }

    /**
     * @throws UserException si $id_related ne correspond pas à un produit
     */
    public function setRelated(int $id_related): Product
    {
        $em = EntityManager::getInstance(Product::class);
        $product = $em->one('SELECT * FROM @TABLE WHERE id=?', $id_related);
        if(!$product) {
            throw new UserException('Le produit lié n\'existe pas');
        }
        $this->id_related = $id_related;
        return $product;
    }

    public function createService(): Service
    {
        $service = new Service();
        $service->label = $this->title;
        $service->description = $this->description;
        $service->save();
        $this->id_service = $service->id;
        return $service;
    }

    public function getRelated(): ?Product
    {
        if(!$this->id_related) {
            return null;
        }
        $em = EntityManager::getInstance(Product::class);
        return $em->one('SELECT * FROM @TABLE WHERE id=?', $this->id_related);
    }
}
