<?php

namespace Garradin\Entities\Communication;

use Garradin\Entity;
use KD2\DB\EntityManager;

class Support extends Entity
{
    const TABLE = 'com_support';

    protected $id;
    protected $title;
    protected $description;
    protected $images_width;
    protected $images_height;
    protected $url;

    protected $_types = [
        'id' => 'int',
        'title' => 'string',
        'description' => '?string',
        'images_width' => '?int',
        'images_height' => '?int',
        'url' => '?string',
    ];

    public function getFields(): array
    {
        if(!$this->exists()) {
            return [];
        }
        $em = EntityManager::getInstance(Field::class);
        return $em->all('SELECT * FROM @TABLE WHERE id_support = ?', $this->id);
    }

    /**
     * @override
     */
    public function selfCheck(): void
    {
        // La hauteur et la largeur doivent être précisées ensemble, si elles le sont
        if(null === $this->images_width) {
            $this->assert(null === $this->images_height, 'La hauteur des images ne peut pas être précisée sans la largeur');
        }
        if(null === $this->images_height) {
            $this->assert(null === $this->images_width, 'La largeur des images ne peut pas être précisée sans la hauteur');
        }
    }
}
