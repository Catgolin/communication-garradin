<?php

namespace Garradin\Entities\Communication;

use Garradin\Entity;
use KD2\DB\EntityManager;

/**
 * Represents an objective of the communication plan
 */
class Objective extends Entity
{
    const TABLE = 'com_objective';

    protected $id;
    protected $title;
    protected $description;
    protected $start;
    protected $end;

    protected $_types = [
        'id' => 'int',
        'title' => 'string',
        'description' => '?string',
        'start' => '?date',
        'end' => '?date',
    ];

    protected $_indicators;
    protected $_old_indicators;

    public static function create(string $title, ?string $description): Objective
    {
        $obj = new self;
        $obj->title = $title;
        $obj->description = $description;
    }

    public function getIndicators(): array
    {
        if(null === $this->_indicators && $this->exists()) {
            $em = EntityManager::getInstance(Indicator::class);
            $this->_indicators = $em->all('SELECT * FROM @TABLE WHERE id_objective = ? ORDER BY id;', $this->id);
        } elseif (null === $this->_indicators) {
            $this->_indicators = [];
        }
        return $this->_indicators;
    }

    public function addIndicator(Indicator $new): void
    {
        $this->_indicators[] = $new;
    }

    public function removeIndicator(Indicator $removed): void
    {
        $new = [];
        foreach ($this->getIndicators() as $indicator) {
            if ($indicator->id === $remove->id) {
                $this->_old_indicators[] = $remove;
            } else {
                $new[] = $indicator;
            }
        }
        $this->_indicators = $new;
    }

    public function resetIndicators(): void
    {
        $this->_old_indicators[] = $this->_indicators;
        $this->_indicators = [];
    }

    public function importForm(?array $source = null): void
    {
        if(null === $source) {
            $source = $_POST;
        }

        parent::importForm($source);

        if(isset($source['start']) && empty($source['start'])) {
            $this->start = null;
        }
        if(isset($source['end']) && empty($source['end'])) {
            $this->end = null;
        }


        if(isset($source['indicators']) && is_array($source['indicators'])) {
            $this->resetIndicators();
            // Add the indicators
            foreach($source['indicators'] as $i => $indicatorForm) {
                $indicator = new Indicator;
                $indicator->importForm($indicatorForm);
                $this->addIndicator($indicator);
            }
        }
    }

    /**
     * @override
     */
    public function selfCheck(): void
    {
        $this->assert($this->title !== null, 'L\'objectif doit avoir un titre afin d\'être identifiable par la suite');
        parent::selfCheck();
    }

    /**
     * As in evry Entity from Garradin,
     * even though I'm not sure I understood why it is there.
     */
    public function asDetailsArray(): array
    {
        $indicators = [];

        foreach($this->getIndicators() as $i => $indicator) {
            $indicators[$i+1] = $indicator->asDetailsArray();
        }

        return [
            'Titre' => $this->title,
            'Description' => $this->description,
            'Indicateurs' => $indicators,
        ];
    }
}
