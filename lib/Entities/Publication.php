<?php

namespace Garradin\Entities\Communication;

use KD2\DB\EntityManager;
use Garradin\Entity;
use Garradin\DB;

class Publication extends Entity
{
    const TABLE = 'com_publication';

    protected $id;
    protected $title;
    protected $id_template;
    protected $id_product;
    protected $id_campaign;
    protected $release_date;
    protected $release_time;

    protected $_types = [
        'id' => 'int',
        'title' => 'string',
        'id_product' => '?int',
        'id_campaign' => 'int',
        'id_template' => 'int',
        'release_date' => '?date',
        'release_time' => '?string',
    ];

    public function getFields(): array
    {
        $em = EntityManager::getInstance(Field::class);
        return $em->all('SELECT * FROM @TABLE WHERE id_template=?', $this->id_template);
    }

    public function getTexts(): array
    {
        if(!$this->exists()) {
            return [];
        }
        $em = EntityManager::getInstance(Text::class);
        return $em->all('SELECT * FROM @TABLE WHERE id_publication=?', $this->id);
    }

    public function getTemplate(): ?Template
    {
        return EntityManager::getInstance(Template::class)
            ->one('SELECT * FROM @TABLE WHERE id=?', $this->id_template);
    }

    public function getCampaign(): ?Campaign
    {
        return EntityManager::getInstance(Campaign::class)
            ->one('SELECT * FROM @TABLE WHERE id=?', $this->id_campaign);
    }

    public function getProduct(): ?Product
    {
        if($this->id_product === null) {
            return $this->getCampaign()->getProduct();
        }
        return EntityManager::getInstance(Product::class)
            ->one('SELECT * FROM @TABLE WHERE id=?', $this->id_product);
    }

    public function getText(Field $field): ?Text
    {
        if($this->exists()) {
            $text = EntityManager::getInstance(Text::class)
                  ->one('SELECT * FROM @TABLE WHERE id_publication=? AND id_field=?', $this->id, $field->id);
            if(null !== $text) {
                return $text;
            }
        }
        $text = new Text();
        $text->id_field = $field->id;
        if($this->exists()) {
            $text->id_publication = $this->id;
        }
        return $text;
    }

    /**
     * @override
     */
    function selfCheck(): void
    {
        $this->assert(null === $this->release_time || null === $this->release_date, 'Vous ne pouvez pas définir une heure de publication sans définir le jour');
    }
}
