<?php

namespace Garradin\Entities\Communication;

use Garradin\Entity;

class Field extends Entity
{
    const TABLE = 'com_field';

    protected $id;
    protected $title;
    protected $description;
    protected $max_length;
    protected $prefered_length;
    protected $id_template;
    protected $id_support;

    protected $_types = [
        'id' => 'int',
        'title' => 'string',
        'description' => '?string',
        'max_length' => '?int',
        'prefered_length' => '?int',
        'id_template' => '?int',
        'id_support' => '?int',
    ];

    /**
     * @override
     */
    public function selfCheck(): void
    {
        $this->assert(null !== $this->id_template || null !== $this->id_support, 'Le champ doit être relié à un template ou à un support');
        $this->assert(null === $this->prefered_length || null === $this->max_length || $this->prefered_length <= $this->max_length);
    }
}
