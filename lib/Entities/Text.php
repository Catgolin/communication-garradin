<?php

namespace Garradin\Entities\Communication;

use Garradin\Entity;

class Text extends Entity
{
    const TABLE = 'com_text';

    protected $id;
    protected $content;
    protected $id_field;
    protected $id_publication;
    protected $is_done = false;

    protected $_types = [
        'id' => 'int',
        'content' => '?string',
        'id_field' => 'int',
        'id_publication' => 'int',
        'is_done' => '?bool',
    ];

    /**
     * @override
     */
    public function selfCheck(): void
    {
        $this->assert(false === $this->is_done || null !== $this->content, 'Le texte ne peut pas être validé s\'il est vide');
    }
}
