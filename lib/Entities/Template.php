<?php

namespace Garradin\Entities\Communication;

use Garradin\Entity;
use Garradin\DB;
use KD2\DB\EntityManager;

class Template extends Entity
{
    const TABLE = 'com_template';
    const SUPPORTS_TABLE = 'com_template_supports';

    protected $id;
    protected $title;
    protected $description;
    protected $id_campaign;

    protected $_types = [
        'id' => 'int',
        'title' => 'string',
        'description' => '?string',
        'id_campaign' => '?int',
    ];

    protected $_supports;

    public function getSupports(): array
    {
        if(null === $this->_supports) {
            $this->_supports = [];
        }
        if($this->exists()) {
            $em = EntityManager::getInstance(Support::class);
            $this->_supports = $em->all(
                <<<EOF
                SELECT * FROM @TABLE WHERE id IN
                (
                    SELECT id_support FROM :table WHERE id_template=:id
                )
                EOF,
                self::SUPPORTS_TABLE,
                $this->id
            );
        }
        return $this->_supports;
    }

    public function addSupport(Support $add): void
    {
        if(!$this->exists()) {
            throw new \Exception('The template should be created before adding a support');
        }
        $db = DB::getInstance();
        $id = $db->firstColumn('SELECT MAX(id) + 1 FROM ?;', [self::SUPPORTS_TABLE]);
        $data = [
            'id' => $id,
            'id_support' => $add->id,
            'id_template' => $this->id,
        ];
        $db->insert(self::SUPPORT_TABLE, $data);
        $this->_supports[] = $add;
    }

    public function removeSupport(Support $removed): void
    {
        if(!$this->exists()) {
            return;
        }
        $db = DB::getInstance();
        $db->delete(
            self::OBJECTIVE_TABLE,
            'id_support = :support AND id_template = :template',
            [
                'support' => $removed->id,
                'template' => $this->id,
            ]
        );
        $this->_supports = null;
        $this->getSupports();
    }
}
