<?php

namespace Garradin\Entities\Communication;

use Garradin\Entity;
use KD2\DB\EntityManager;

class Campaign extends Entity
{
    const TABLE = 'com_campaign';
    const OBJECTIVES_TABLE = 'com_campaign_objectives';

    protected $id;
    protected $title;
    protected $id_product;
    protected $id_primary_target;
    protected $description;
    protected $start;
    protected $end;

    protected $_types = [
        'id' => 'int',
        'title' => 'string',
        'id_product' => 'int',
        'id_primary_target' => 'int',
        'description' => '?string',
        'start' => '?date',
        'end' => '?date',
    ];

    protected $_objectives;
    protected $_old_objectives = [];

    public static function create(string $title, int $id_product, int $id_primary_target, ?string $description = null, ?\DateTime $start = null, ?\DateTime $end = null): Campaign
    {
        $camp = new salf;
        $camp->title = $title;
        $camp->id_product = $id_product;
        $camp->id_primary_target = $id_primary_target;
        $camp->description = $description;
        $camp->start = $start;
        $camp->end = $end;
    }

    public function getObjectives(): array
    {
        if(!$this->exists()) {
            $this->_objectives = [];
            return [];
        }

        if(null === $this->_objectives) {
            // TODO: get objectives id
            $objectives_id = [];
            $em = EntityManager::getInstance(Objective::class);
            foreach($objectives_id as $id) {
                $this->_objectives[] = $em->one('SELECT * FROM @TABLE WHERE id = ?', $id);
            }
        }

        return $this->_objectives;
    }

    public function addObjective(Ojective $objective): void
    {
        if(!$this->exists()) {
            throw new \Exception('The campaign should be created before adding objectives');
        }
        $db = DB::getInstance();
        $id = $db->firstColumn('SELECT MAX(id) + 1 FROM ?;', [self::OBJECTIVES_TABLE]);
        $data = [
            'id' => $id,
            'id_objective' => $objective->id,
            'id_campaign' => $campagin->id,
        ];
        $db->insert(self::OBJECTIVES_TABLE, $data);
        $this->_objectives[] = $objective;
    }

    public function removeObjective(Objective $removed): void
    {
        if(!$this->exists()) {
            return;
        }
        $this->_objectives = [];
        foreach($this->getObjectives() as $obj) {
            if($obj->id !== $removed->id) {
                $this->_objectives[] = $obj;
            }
        }
        $db = DB::getinstance();
        $db->delete(
            self::OBJECTIVES_TABLE,
            'id_objective = :obj AND id_campaign = :camp',
            [
                'obj' => $removed->id,
                'camp' => $this->id,
            ]
        );
    }

    public function getProduct(): ?Product
    {
        return EntityManager::getInstance(Product::class)
            ->one('SELECT * FROM @TABLE WHERE id=?', $this->id_product);
    }

    public function getPrimaryTarget(): ?Segment
    {
        return EntityManager::getInstance(Segment::class)
            ->one('SELECT * FROM @TABLE WHERE id=?', $this->id_primary_target);
    }

    /**
     * @override
     */
    public function selfCheck(): void
    {
        $segmentEm = EntityManager::getInstance(Segment::class);
        $productEm = EntityManager::getInstance(Product::class);

        $primaryTarget = $segmentEm->one('SELECT * FROM @TABLE WHERE id=?', $this->id_primary_target);
        $this->assert($primaryTarget !== null, 'La cible principale doit être définie');

        $product = $productEm->one('SELECT * FROM @TABLE WHERE id=?', $this->id_product);
        $this->assert(null !== $product, 'Le produit doit être défini');

        parent::selfCheck();
    }
}
