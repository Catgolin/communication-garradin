<?php

namespace Garradin\Communication;

use KD2\DB\AbstractEntity;
use KD2\DB\EntityManager;
use Garradin\Form;
use Garradin\Template;
use Garradin\Utils;
use Garradin\UserException;

/**
 * Génère les pages pour éditer, afficher ou supprimer des entités.
 */
class Pages
{
    protected string $class_name;
    protected string $entity_name;

    protected Template $tpl;

    protected string $template_path;
    protected string $pages_path;

    /**
     * @param string $class_name Le nomo complet de la classe de l'entité à gérer
     */
    public function __construct(string $class_name)
    {
        $this->class_name = $class_name;

        $name = explode('\\', $this->class_name);
        $name = end($name);
        $this->entity_name = strtolower($name);

        $this->tpl = Template::getInstance();

        $this->tpl_path = \Garradin\PLUGIN_ROOT . '/templates/entities/' . $this->entity_name . 's/';
        $this->pages_path = \Garradin\PLUGIN_URL . $this->entity_name . 's/';
    }

    /**
     * @brief Affiche une page qui détaille les informations de l'entité
     *
     * @param ?callable $specific_action Fonction à appeler après le chargement
     * de l'entité à afficher, pour personnaliser la page.
     * $specific_action prend en paramètre l'entité affichée.
     *
     * @throws UserException si l'entité demandée n'existe pas.
     */
    public function display(?callable $specific_actions = null): void
    {
        $em = EntityManager::getInstance($this->class_name);
        $entity = $em->one('SELECT * FROM @TABLE WHERE id=?', \Garradin\qg('id'));
        if(!$entity) {
            throw new UserException('L\'objet avec l\'identifiant ' . \Garradin\qg('id') . 'n\'existe pas');
        }
        $this->tpl->assign($this->entity_name, $entity);

        if(null !== $specific_actions) {
            $specific_actions($entity);
        }

        $this->tpl->display($this->tpl_path . 'display.tpl');
    }

    /**
     * @brief Affiche une page qui permet de créer ou de modifier l'entité
     *
     * @param ?callable $load_options Fonction à appeler au chargement de la page
     *
     * @param ?callable $after_save Fonction à appeler après l'enregistrement de l'entité
     *
     * @param ?callable $specific_action Fonction à appeler
     * juste avant d'afficher la page.
     *
     * @note Les différentes fonctions prennent l'entité à éditer en paramètre.
     */
    public function edit(array $required_options = [], array $optionnal_options = [], ?callable $before_save = null, ?callable $after_save = null, ?callable $specific_actions = null): void
    {
        $em = EntityManager::getInstance($this->class_name);
        $entity = $em->one('SELECT * FROM @TABLE WHERE id=?', \Garradin\qg('id'));
        if(!$entity) {
            $entity = new $this->class_name();
        }

        foreach($required_options as $name => $class) {
            $attribute = 'id_' . $name;
            $id = (int) \Garradin\qg($name);
            if(!$id) {
                if(!$entity->$attribute) {
                    throw new UserException('Option(s) manquante(s)');
                }
                $id = $entity->$attribute;
            }
            if(null === $this->setEntity($name, $id, $class)) {
                throw new UserException('Option(s) invalide(s)');
            }
            $entity->$attribute = (int) $id;
        }

        foreach($optionnal_options as $name => $class) {
            $attribute = 'id_' . $name;
            $id = \Garradin\qg($name);
            if($id) {
                $entity->$attribute = (int) $id;
            }
        }

        $form = new Form();

        if(\Garradin\f('save') && $form->check('edit_' . $this->entity_name) && !$form->hasErrors()) {
            $entity->importForm();

            if(null !== $before_save) {
                $before_save($entity);
            }

            $entity->save();

            if(null !== $after_save) {
                $after_save($entity);
            }

            Utils::redirect($this->pages_path . 'display.php?id=' . $entity->id);
        }

        $this->tpl->assign($this->entity_name, $entity);

        if(null !== $specific_actions) {
            $specific_actions($entity);
        }

        $this->tpl->display($this->tpl_path . 'edit.tpl');
    }

    /**
     * @brief Affiche la page pour supprimer une entité
     *
     * @param ?callable $before_delete Fonction à appeler
     * avant la suppression de l'entité.
     *
     * @param ?callable $specific_action Fonction à appeler
     * avant d'afficher la page.
     *
     * @throws UserException si l'entité n'existe pas.
     *
     * @note Les fonctions prennent en paramètre l'entité à supprimer.
     */
    public function delete(?callable $before_delete = null, ?callable $specific_actions = null): void
    {
        $em = EntityManager::getInstance($this->class_name);
        $entity = $em->one('SELECT * FROM @TABLE WHERE id=?', \Garradin\qg('id'));
        if(!$entity) {
            throw new UserException('L\'objet avec l\'identifiant ' . \Garradin\qg('id') . 'n\'existe pas');
        }
        $this->tpl->assign($this->entity_name, $entity);

        $form = new Form();

        if(\Garradin\f('delete') && $form->check('com_delete_' . $this->entity_name . '_' . $entity->id)) {
            try {
                if($before_delete) {
                    $before_delete($entity);
                }
                $entity->delete();
                Utils::redirect(\Garradin\PLUGIN_URL);
            } catch(UserException $e) {
                $form->addError($e->getMessage());
            }
        }

        if($specific_actions) {
            $specific_actions($entity);
        }

        $this->tpl->display($this->tpl_path . 'delete.tpl');
    }

    /**
     * @param string $attribute Le nom de l'entité
     * @param int $property L'identifiant de l'entité
     * @param string $classname Le nom de la class de l'entité
     */
    public function setEntity(string $attribute, ?int $id, string $className): ?AbstractEntity
    {
        if(null === $id) {
            $this->tpl->assign($attribute, null);
            return null;
        }
        $em = EntityManager::getInstance($className);
        $entity = $em->one('SELECT * FROM @TABLE WHERE id=?', $id);
        $this->tpl->assign($attribute, $entity);
        return $entity;
    }

    public function setEntitiesAsArray(string $className, string $name): array
    {
        $em = EntityManager::getInstance($className);
        $entities = $em->all('SELECT * FROM @TABLE');
        $res = [];
        foreach($entities as $entity) {
            $res[$entity->id] = $entity->title;
        }
        $this->tpl->assign($name, $res);
        return $res;
    }
}
