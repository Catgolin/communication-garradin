<nav class="tabs noprint">
  {if $level == 'strategy'}
  <aside>
    {linkbutton href='%ssupports/edit.php'|args:$plugin_url label="Ajouter un support" shape="plus"}
    {linkbutton href='%ssegments/edit.php'|args:$plugin_url label="Ajouter un segment" shape="plus"}
  </aside>
  {/if}
  <ul>
    <li{if $level == 'strategy'} class="current"{/if}>
      <a href="{plugin_url file="index.php"}">Stratégie de commmunication</a>
    </li>
    <li{if $level == 'tactics'} class="current"{/if}>
      <a href="{plugin_url file="campaigns.php"}">Produits et campagnes en cours</a>
    </li>
    <li{if $level == 'operation'} class="current"{/if}>
      <a href="{plugin_url file="calendar.php"}">Calendrier éditorial</a>
    </li>
  </ul>
</nav>
