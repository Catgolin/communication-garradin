{form_errors}

<form method="post" action="{$self_url}" enctype="multipart/form-data">
  <fieldset>
    <legend>Préparation de la publication</legend>
    <dl>
      {input type="text" name="title" label="Titre" required=1 source=$publication}
      {input type="date" name="release_date" label="Date de publication" source=$publication}
      {input type="time" name="release_time" label="Heure de publication" source=$publication}
    </dl>
  </fieldset>
  <fieldset>
    <legend>Rédaction de la publication</legend>
    <dl>
      {foreach from=$publication->getFields() item="field"}
      {input type="text" name="field_%s"|args:$field.id label=$field.title default=$publication->getText($field)->content}
      {/foreach}
    </dl>
  </fieldset>
  <p class="submit">
    {csrf_field key="edit_publication"}
    {button type="submit" name="save" label="Enregistrer" shape="right" class="main}
  </p>
</form>
