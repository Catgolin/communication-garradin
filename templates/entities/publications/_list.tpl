{if !empty($list)}
<table class="list">
  <thead>
    <tr>
      <th class="num">N°</th>
      <th>Titre</th>
      <th>Modèle</th>
      <th>Date</th>
      <th>Campagne</th>
      <th>Produit</th>
      <th>Actions</th>
  </thead>
  <tbody>
    {foreach from=$list item="publication"}
    <tr>
      <td class="num"><a href="{plugin_url file="publications/display.php?id=%s"|args:$publication.id}">{$publication.id}</a></td>
      <td>{$publication.title}</td>
      <td>
        <a href="{plugin_url file="templates/display.php?id=%s"|args:$publication.id_template}">
          {$publication->getTemplate()->title}
        </a>
      </td>
      <td>
        {if $publication.release_date}
        {$publication.release_date|date:'d/m/Y'}
        {if $publication.release_time}
        {$publicaiton.release_time}
        {/if}
        {/if}
      </td>
      <td{if !$publication->id_product} colspan="2"{/if}>
        <a href="{plugin_url file="campaigns/display.php?id=%s"|args:$publication.id_campaign}">
          {$publication->getCampaign()->title}
        </a>
      </td>
      {if $publication->id_product}
      <td>
        <a href="{plugin_url file="products/display.php?id=%s"|args:$publication.id_product}">
          {$publication->getProduct()->title}
        </a>
      </td>
      {/if}
      <td>
        {linkbutton href="%spublications/edit.php?id=%s"|args:$plugin_url,$publication.id shape="edit" label="Éditer"}
        {linkbutton href="%spublications/delete.php?id=%s"|args:$plugin_url,$publication.id shape="delete" label="Supprimer"}
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}
