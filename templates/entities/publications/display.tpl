{include file="admin/_head.tpl" title='Publication : %s'|args:$publication.title current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="operation"}

<dl class="describe">
  <dt>
    {linkbutton href="%spublications/edit.php?id=%s"|args:$plugin_url,$publication.id shape="edit" label="Éditer"}
    {linkbutton href="%spublications/delete.php?id=%s"|args:$plugin_url,$publication.id shape="delete" label="Supprimer"}
  </dt>
  <dd>
    <h2>{$publication.title}</h2>
  </dd>
  <dt>Template</dt>
  <dd>
    <a href="{plugin_url file="templates/display.php?id=%s"|args:$publication.id_template}">
      {$publication->getTemplate()->title}
    </a>
  </dd>
  <dt>Campagne</dt>
  <dd>
    <a href="{plugin_url file="campaigns/display.php?id=%s"|args:$publication.id_campaign}">
      {$publication->getCampaign()->title}
    </a>
  </dd>
  {if $publication.id_product}
  <dt>Produit</dt>
  <dd>
    <a href="{plugin_url file="products/display.php?id=%s"|args:$publication.id_product">
      {$publication->getProduct()->title}
    </a>
  </dd>
  {/if}
</dl>

<dl class="describe">
  {foreach from=$publication->getFields() item="field"}
  <dt>{$field.title}</dt>
  <dd>{$publication->getText($field)->content}</dd>
  {/foreach}
</dl>

{include file="admin/_foot.tpl"}
