{include file="admin/_head.tpl" title="Supprimer la publication  « %s »"|args:$publication.title current="plugin_%s"|args:$plugin.id}

{include file="common/delete_form.tpl"
         legend="Supprimer cette publication ?"
         warning="Êtes-vous sûr·e de vouloir supprimer la publication « %s » ?"|args:$publication.title
         csrf_key="com_delete_publication_%s"|args:$publication.id}

{include file="admin/_foot.tpl"}
