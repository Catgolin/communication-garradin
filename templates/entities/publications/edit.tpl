{include file="admin/_head.tpl" title="Préparer une publication" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="tactics"}

{include file="%s/templates/entities/publications/_form.tpl"|args:$plugin_root publication=$publication}

{include file="admin/_foot.tpl"}