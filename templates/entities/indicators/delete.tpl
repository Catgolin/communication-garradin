{include file="admin/_head.tpl" title="Supprimer l'indicateur « %s »"|args:$indicator.title current="plugin_%s"|args:$plugin.id}

{include file="common/delete_form.tpl"
         legend="Supprimer cet indicateur ?"
         warning="Êtes-vous sûr·e de vouloir supprimer l'indicateur « %s »"|args:$indicator.title
         csrf_key="com_delete_indicator_%s"|args:$indicator.id}

{include file="admin/_foot.tpl"}