{include file="admin/_head.tpl" title="Définir un indicateur pour l'objectif « %s »"|args:$objective.title current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="strategy"}

{include file="%s/templates/entities/indicators/_form.tpl"|args:$plugin_root indicator=$indicator}

{include file="admin/_foot.tpl"}