{if !empty($list)}
<table class="list">
  <thead>
    <tr>
      <th class="num">N°</th>
      <th>Intitulé</th>
      <th>Date limite</th>
      <th>Avancement</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$list item="indicator"}
    <?php $percent = $indicator->percentage(); ?>
    <tr>
      <td class="num"><a href="{plugin_url file="indicators/display.php?id=%s"|args:$indicator.id}">{$indicator.id}</a></td>
      <td>{$indicator.title}</td>
      <td>{$indicator.deadline|date:'l j F Y (d/m/Y)'}</td>
      <td>
        {$indicator.value}/{$indicator.target} {$indicator.metric}
        {if $percent} ({$percent}%) {/if}
      </td>
      <td>
        {linkbutton href="%sindicators/edit.php?id=%s"|args:$plugin_url,$indicator.id shape="edit" label="Modifier"}
        {linkbutton href="%sindicators/delete.php?id=%s"|args:$plugin_url,$indicator.id shape="delete" label="Supprimer"}
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}
