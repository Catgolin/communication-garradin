{form_errors}

<form method="post" action="{$self_url}" enctype="multipart/form-data">
  <fieldset>
    <legend>Indicateur</legend>
    <dl>
      {input type="text" name="title" label="Titre" required=1 source=$indicator}
      {input type="textarea" name="description" label="Description" source=$indicator}
      {input type="text" name="target" label="Valeur cible pour cet indicateur" source=$indicator}
      {input type="text" name="value" label="Valeur actuelle de l'indicateur" source=$indicator}
      {input type="text" name="metric" label="Unité de mesure de l'indicateur" source=$indicator}
      {input type="date" name="deadline" label="Date limite pour la réalisation de l'indicateur" source=$indicator}
    </dl>
    <p class="submit">
      {csrf_field key="edit_objective"}
      {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    </p>
  </fieldset>
</form>
