{form_errors}

<form method="post" action="{$self_url}" enctype="multipart/form-data">
  <fieldset>
    <legend>Template ou format de communication</legend>
    <dl>
      {input type="text" name="title" label="Titre" required=1 source=$template}
      {input type="textarea" name="description" label="Description" source=$template}
    </dl>
    <p class="submit">
      {csrf_field key="edit_template"}
      {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    </p>
  </fieldset>
</form>
