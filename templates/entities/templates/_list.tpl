{if !empty($list)}
<table class="list">
  <thead>
    <tr>
      <th class="num">N°</th>
      <th>Libellé</th>
      <th>Support(s)</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$list item="template"}
    <tr>
      <td class="num"><a href="{plugin_url file="templates/display.php?id=%s"|args:$template.id}">{$template.id}</a></td>
      <td>
        <a href="{plugin_url file="templates/display.php?id=%s"|args:$template.id}">
          {$template.title}
        </a>
      </td>
      <td></td>
      <td>
        {if isset($campaign) && $campaign}
        {linkbutton href="%spublications/edit.php?template=%s&campaign=%s"|args:$plugin_url,$template.id,$campaign.id shape="plus" label="Prévoir une publication"}
        {else}
        {linkbutton href="%spublications/edit.php?template=%s"|args:$plugin_url,$template.id shape="plus" label="Prévoir une publication"}
        {/if}
        <br/>
        {linkbutton href="%stemplates/edit.php?id=%s"|args:$plugin_url,$template.id shape="edit" label="Éditer"}
        {linkbutton href="%stemplates/delete.php?id=%s"|args:$plugin_url,$template.id shape="delete" label="Supprimer"}
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}
