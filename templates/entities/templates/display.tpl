{include file="admin/_head.tpl" title='Template : %s'|args:$template.title current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="tactics"}

<dl class="describe">
  <dt>
    {linkbutton href="%stemplates/edit.php?id=%s"|args:$plugin_url,$template.id shape="edit" label="Éditer"}
    {linkbutton href="%stemplates/delete.php?id=%s"|args:$plugin_url,$template.id shape="delete" label="Supprimer"}
  </dt>
  <dd>
    <h2>{$template.title}</h2>
  </dd>
  {if $template.description}
  <dt>Description</dt>
  <dd>{$template.description}</dd>
  {/if}
  {if $template.id_campaign}
  <dt>Template lié à la campagne</dt>
  <dd>
    <a href="{"%scampaigns/display.php?id=%s"|args:$plugin_url,$template.id_campaign}">
      {$campaign.title}
    </a>
  </dd>
  {if $campaign.description}
  <dd class="help">{$campaign.description}</dd>
  {/if}
  {/if}
</dl>

<dl class="describe">
  <dt><h3>Champs pour les publications</h3></dt>
  <dd>Textes à rédiger pour les publications suivant ce format</dd>
  <dt>
    {linkbutton href="%sfields/edit.php?template=%s"|args:$plugin_url,$template.id shape="plus" label="Ajouter un champ"}
  </dt>
  <dd>
    {include file="%s/templates/entities/fields/_list.tpl"|args:$plugin_root list=$fields}
  </dd>
</dl>

<dl class="describe">
  <dt><h3>Publication</h3></dt>
  <dd>Publications programmées à partir de ce format</dd>
  <dt>
    {linkbutton href=$link_create shape="plus" label="Prévoir une publication"}
  </dt>
  <dd>
    {include file="%s/templates/entities/publications/_list.tpl"|args:$plugin_root list=$publications}
  </dd>
</dl>

{include file="admin/_foot.tpl"}
