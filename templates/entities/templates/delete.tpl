{include file="admin/_head.tpl" title="Supprimer le template « %s »"|args:$template.title current="plugin_%s"|args:$plugin.id}

{include file="common/delete_form.tpl"
         legend="Supprimer ce template ?"
         warning="Êtes-vous sûr·e de vouloir supprimer le template « %s » et tous les champs s'y rattachant ?"|args:$template.title
         csrf_key="com_delete_template%s"|args:$template.id}

{include file="admin/_foot.tpl"}
