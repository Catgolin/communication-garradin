{include file="admin/_head.tpl" title="Définir un modèle de publication" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="tactics"}

{include file="%s/templates/entities/templates/_form.tpl"|args:$plugin_root template=$template}

{include file="admin/_foot.tpl"}
