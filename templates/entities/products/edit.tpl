{include file="admin/_head.tpl" title="Définir un produit" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="tactics"}

{include file="%s/templates/entities/products/_form.tpl"|args:$plugin_root product=$product}

{include file="admin/_foot.tpl"}