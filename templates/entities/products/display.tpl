{include file="admin/_head.tpl" title='%s'|args:$type current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="tactics"}

<dl class="describe">
  <dt>
    {linkbutton href="%sproducts/edit.php?id=%s"|args:$plugin_url,$product.id shape="edit" label="Éditer"}
    {linkbutton href="%sproducts/delete.php?id=%s"|args:$plugin_url,$product.id shape="delete" label="Supprimer"}
  </dt>
  <dd>
    <h2>{$product.title}</h2>
  </dd>
  {if $product.description}
  <dt>Description</dt>
  <dd>{$product.description}</dd>
  {/if}

  {if $product.id_related}
  <dt>Gamme</dt>
  <dd>
    <a href="{plugin_url file="products/display.php?id=%s"|args:$product.id_related}">
      {$product->getRelated()->title}
    </a>
  </dd>
  {/if}

  {if $product.type == $product::TYPE_EVENT}
  {if $product.date_event}
  <dt>Date de l'événement</dt>
  <dd>{$product.date_event|date:'l j F Y (d/m/Y)'}</dd>
  {/if}
  {if $product.time_start}
  <dt>Heure de début</dt>
  <dd>{$product.time_start}</dd>
  {/if}
  {if $product.time_end}
  <dt>Heure de fin</dt>
  <dd>{$product.time_end}</dd>
  {/if}
  {/if}

  {if $product.type == $product::TYPE_DURATION}
  <dt>Début</dt>
  <dd>{$product.start|date:'l j F Y (d/m/Y)'}</dd>
  <dt>Fin</dt>
  <dd>{$product.end|date:'l j F Y (d/m/Y)'}</dd>
  {/if}

  {if $product.action}
  <dt>Informations pratiques</dt>
  <dd>{$product.action}</dd>
  {/if}
  {if $product.url}
  <dt>Lien</dt>
  <dd><a href="{$product.url}">{$product.url}</a></dd>
  {/if}
  {if $product.deadline}
  <dt>Date limite pour s'inscrire</dt>
  <dd>{$product.deadline|date:'l j F Y (d/m/Y)'}</dd>
  {/if}
</dl>

{if $service}
<dl class="describe">
  <dt>Activité liée</dt>
  <dd>
    <a href="/admin/services/fees/?id={$service.id}">{$service.label}</a>
  </dd>
</dl>
{/if}

<dl class="describe">
  <dt><h3>Campagnes associées</h3></dt>
  <dd>Plan de communication pour ce produit</dd>
  <dt>
    {linkbutton href="%scampaigns/edit.php?product=%s"|args:$plugin_url,$product.id shape="plus" label="Définir une campagne"}
  </dt>
  <dd>
    {include file="%s/templates/entities/campaigns/_list.tpl"|args:$plugin_root list=$campaigns}
  </dd>
</dl>

<dl class="describe">
  <dt><h3>Produits liés</h3></dt>
  <dd>Il est possible de définir une gamme de produits rattachés à celui-ci.</dd>
  <dt>
    {linkbutton href="%sproducts/edit.php?related=%s"|args:$plugin_url,$product.id shape="plus" label="Ajouter un produit lié"}
  </dt>
  <dd>
    {include file="%s/templates/entities/products/_list.tpl"|args:$plugin_root list=$children}
  </dd>
</dl>

{include file="admin/_foot.tpl"}
