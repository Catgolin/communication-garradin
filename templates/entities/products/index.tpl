{include file="admin/_head.tpl" title="Événements et produits à promouvoir" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root current="products"}

{linkbutton href="%sproducts/edit.php"|args:$plugin_url shape="plus" label="Ajouter un nouvel évènement"}

{if $durations && count($durations) > 0}
<table class="list">
  <thead>
    <tr>
      <td class="num">N°</td>
      <th>Titre</th>
      <th>Date limite d'action</th>
      <th>Date de début</th>
      <th>Date de fin</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$durations item="product"}
    <tr>
      <td class="num">
        <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$product.id}">{$product.id}</a>
      </td>
      <td>
        <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$product.id}">
          {$product.title}
        </a>
      </td>
      <td>{$product.deadline|date:'d/m/Y'}</td>
      <td>{$product.start|date:'d/m/Y'}</td>
      <td>{$product.end|date:'d/m/Y'}</td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

{if $events && count($events) > 0}
<h2>Événements</h2>

<table class="list">
  <thead>
    <tr>
      <td class="num">N°</td>
      <th>Titre</th>
      <th>Date limite</th>
      <th>Date de l'événement</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$events item="product"}
    <tr>
      <td class="num">
        <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$product.id}">{$product.id}</a>
      </td>
      <td>
        <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$product.id}">
          {$product.title}
        </a>
      </td>
      <td>{$product.deadline|date:'d/m/Y'}</td>
      <td>{$product.date_event|date:'d/m/Y'}</td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

{if $products && count($products) > 0}
<h2>Produits</h2>

<table class="list">
  <thead>
    <tr>
      <td class="num">N°</td>
      <th>Titre</th>
      <th>Deadline</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$products item="product"}
    <tr>
      <td class="num">
        <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$product.id}">{$product.id}</a>
      </td>
      <td>
        <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$product.id}">
          {$product.title}
        </a>
      </td>
      <td>{$product.deadline|date:'d/m/Y'}</td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

{if $past_events && count($past_events) > 0}
<h2>Événements passés</h2>
<table class="list">
  <thead>
    <tr>
      <td class="num">N°</td>
      <th>Titre</th>
      <th>Date</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$past_events item="product"}
    <tr>
      <td class="num">
        <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$product.id}">{$product.id}</a>
      </td>
      <td>
        <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$product.id}">
          {$product.title}
        </a>
      </td>
      <td>{$product.date_event|date:'d/m/Y'}</td>
      <td>
        {linkbutton href="%sproducts/delete.php?id=%s"|args:$plugin_url,$product.id shape="delete" label="Supprimer"}
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

{include file="admin/_foot.tpl"}
