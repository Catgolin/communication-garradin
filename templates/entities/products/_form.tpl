{form_errors}

<form method="post" action="{$self_url}" enctype="multipart/form-data">
  <fieldset>
    <legend>Type de produit</legend>
    <dl>
      {foreach from=$types item="type"}
      <dd class="radio-btn">
        {input type="radio" name="type" value=$type.id source=$product label=null}
        <label for="f_type_{$type.id}">
          <div>
            <h3>{$type.label}</h3>
            {if !empty($type.help)}
            <p class="help">{$type.help}</p>
            {/if}
          </div>
        </label>
      </dd>
      {/foreach}
    </dl>
  </fieldset>
  <fieldset data-types="t{$product::TYPE_DURATION}">
    <legend>Durée</legend>
    <dl>
      {input type="date" name="start" label="Date de début" source=$product required=1}
      {input type="date" name="end" label="Date de fin" source=$product required=1}
    </dl>
  </fieldset>
  <fieldset data-types="t{$product::TYPE_EVENT}">
    <legend>Date de l'événement</legend>
    <dl>
      {input type="date" name="date_event" label="Date de l'événement" source=$product required=1}
      {input type="time" name="time_start" label="Début de l'événement" source=$product}
      {input type="time" name="time_end" label="Fin de l'événement" source=$product}
    </dl>
  </fieldset>
  <fieldset>
    <legend>Produit</legend>
    <dl>
      {input type="text" name="title" label="Titre" required=1 source=$product}
      {input type="textarea" name="description" label="Description" source=$product}
      {input type="textarea" name="action" label="Informations pratiques" source=$product}
      {input type="url" name="url" label="Lien" source=$product}
      {input type="date" name="deadline" label="Date limite pour s'inscrire, réserver, etc." source=$product}
    </dl>
  </fieldset>
  <fieldset>
    <legend>Lier à une activité sur Garradin</legend>
    <dl>
      {input type="select" name="id_service" label="Activité associée" options=$services default=$product.id_service}
      {if $product.service === null}
      {input type="checkbox" name="create_service" value="1" label="Créer une activité associée sur Garradin"}
      {/if}
    </dl>
  </fieldset>
  <p class="submit">
    {csrf_field key="edit_product"}
    {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
  </p>
</form>

<script type="text/javascript" async="async">
  {literal}
  // Hide type specific parts of the form
  function hideAllTypes() {
      g.toggle('[data-types]', false);
  }

  // Toggle parts of the form when a type is selected
  function selectType(v) {
      hideAllTypes();
      g.toggle('[data-types~=t' + v + ']', true);
      g.toggle('[data-types=all-but-advanced]', v != 0);
      // Disable required form elements, or the form won't be able to be submitted
      $('[data-types=all-but-advanced] input[required]').forEach((e) => {
          e.disabled = v == 0 ? true : false;
      });
  }

  var radios = $('fieldset input[type=radio][name=type]');

  radios.forEach((e) => {
      e.onchange = () => {
          document.querySelectorAll('fieldset').forEach((e, k) => {
              if (k == 0 || e.dataset.types) return;
              g.toggle(e, true);
              g.toggle('p.submit', true);
          });
          selectType(e.value);
      };
  });

  hideAllTypes();

  // In case of a pre-filled form: show the correct part of the form
  var current = document.querySelector('input[name=type]:checked');
  if (current) {
      selectType(current.value);
  }
  {/literal}
</script>
