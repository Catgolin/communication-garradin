{include file="admin/_head.tpl" title='Supprimer un produit ou événement' current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root current="products"}


{include file="common/delete_form.tpl"
	legend="Supprimer « %s »"|args:$product.title
    warning="Êtes vous sûr de vouloir supprimer cet évènement ou produit ?"
    confirm="Supprimer également les produits reliés"
	csrf_key="com_delete_product_%s"|args:$product.id
}

{include file="admin/_foot.tpl"}