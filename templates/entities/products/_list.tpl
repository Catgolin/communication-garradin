{if !empty($list)}
<table class="list">
  <thead>
    <tr>
      <th class="num">N°</th>
      <th>Libellé</th>
      <th>Type</th>
      <th>Date limite</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$list item="product"}
    <tr>
      <td class="num"><a href="{plugin_url file="products/display.php?id=%s"|args:$product.id}">{$product.id}</a></td>
      <td>
        <a href="{plugin_url file="products/display.php?id=%s"|args:$product.id}">
          {$product.title}
        </a>
      </td>
      <td><?= $product::TYPES[$product->type]['label'] ?></td>
      <td>{$product.deadline|date:'d/m/Y'}</td>
      <td>
        {linkbutton href="%sproducts/edit.php?id=%s"|args:$plugin_url,$product.id shape="edit" label="Éditer"}
        {linkbutton href="%sproducts/delete.php?id=%s"|args:$plugin_url,$product.id shape="delete" label="Supprimer"}
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}
