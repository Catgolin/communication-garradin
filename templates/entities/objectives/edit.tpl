{include file="admin/_head.tpl" title="Fixer un objectif" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="strategy"}

{include file="%s/templates/entities/objectives/_form.tpl"|args:$plugin_root objective=$objective}

{include file="admin/_foot.tpl"}
