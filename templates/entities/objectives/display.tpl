{include file="admin/_head.tpl" title='Objectif : %s'|args:$objective.title current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="strategy"}

<dl class="describe">
  <dt>
    {linkbutton href="%sobjectives/edit.php?id=%s"|args:$plugin_url,$objective.id shape="edit" label="Éditer"}
    {linkbutton href="%sobjectives/delete.php?id=%s"|args:$plugin_url,$objective.id shape="delete" label="Supprimer"}
  </dt>
  <dd>
    <h2>{$objective.title}</h2>
  </dd>
  {if $objective.description}
  <dt>Description</dt>
  <dd>{$objective.description}</dd>
  {/if}
  {if $objective.start}
  <dt>Début de l'objectif</dt>
  <dd>{$objective.start|date:'l j F Y (d/m/Y)'}</dd>
  {/if}
  {if $objective.end}
  <dt>Fin de l'objectif</dt>
  <dd>{$objective.end|date:'l j F Y (d/m/Y)'}</dd>
  {/if}
</dl>

<dl class="describe">
  <dt><h3>Indicateurs</h3></dt>
  <dd>Indicateurs permettant de mesurer l'avancement de l'objectif.</dd>
  <dt>
    {linkbutton href="%sindicators/edit.php?objective=%s"|args:$plugin_url,$objective.id shape="plus" label="Ajouter un indicateur"}
  </dt>
  <dd>
    {include file="%s/templates/entities/indicators/_list.tpl"|args:$plugin_root list=$objective->getIndicators()}
  </dd>
</dl>
