{if !empty($list)}
<table class="list">
  <thead>
    <tr>
      <th class="num">N°</th>
      <th>Intitulé</th>
      <th>Actions</th>
      <th>Indicateurs</th>
      <th>Avancement</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$list item="objective"}
    <tr>
      <?php
       $indicators = $objective->getIndicators();
       $n = count($indicators);
      ?>
      <td class="num"{if $n > 1} rowspan="{$n}"{/if}><a href="{plugin_url file="objectives/display.php?id=%s"|args:$objective.id}">{$objective.id}</a></td>
      <td{if $n > 1} rowspan="{$n}"{/if}>
        <a href="{plugin_url file="objectives/display.php?id=%s"|args:$objective.id}">
          {$objective.title}
        </a>
      </td>
      <td{if $n > 1} rowspan="{$n}"{/if}>
        {linkbutton href="%sobjectives/edit.php?id=%s"|args:$plugin_url,$objective.id shape="edit" label="Éditer"}
      </td>
      {if $n == 0}
      <td colspan="2">
        {linkbutton href="%sindicators/edit.php?objective=%s"|args:$plugin_url,$objective.id shape="plus" label="Définir un indicateur"}
      </td>
      {else}
      {foreach from=$indicators item="indicator" key="i"}
      <?php $percent = $indicator->percentage(); ?>
      <td>{$indicator.title}</td>
      <td>
        {$indicator.value}/{$indicator.target} {$indicator.measure}
        {if $percent} ({$percent}%) {/if}
      </td>
      {if $i < $n}
    </tr>
    <tr>
      {/if}
      {/foreach}
      {/if}
    </tr>
    {/foreach}
  </tbody>
  <tfoot>
    <tr>
      <td colspan="5" class="actions">
        {linkbutton href="%sobjectives/edit.php"|args:$plugin_url shape="plus" label="Ajouter un nouvel objectif"}
      </td>
    </tr>
  </tfoot>
</table>
{/if}
