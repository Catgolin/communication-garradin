{form_errors}

<form method="post" action="{$self_url}" enctype="multipart/form-data">
  <fieldset>
    <legend>Objectif</legend>
    <dl>
      {input type="text" name="title" label="Titre" required=1 source=$objective}
      {input type="textarea" name="description" label="Description" source=$objective}
      {input type="date" name="start" label="Début de l'objectif" source=$objective}
      {input type="date" name="end" label="Fin de l'objectif" source=$objective}
    </dl>
    <p class="submit">
      {csrf_field key="edit_objective"}
      {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    </p>
  </fieldset>
</form>
