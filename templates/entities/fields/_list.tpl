{if !empty($list)}
<table class="list">
  <thead>
    <tr>
      <th>Libellé</th>
      <th>Taille maximale</th>
      <th>Taille recommandée</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$list item="field"}
    <tr>
      <td>{$field.title}</td>
      <td>{$field.max_length}</td>
      <td>{$field.prefered_length}</td>
      <td>
        {linkbutton href="%sfields/edit.php?id=%s"|args:$plugin_url,$field.id shape="edit" label="Modifier"}
        {linkbutton href="%sfields/delete.php?id=%s"|args:$plugin_url,$field.id shape="delete" label="Supprimer"}
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}
