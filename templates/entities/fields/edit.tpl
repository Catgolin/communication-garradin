{include file="admin/_head.tpl" title="Définir un champ de texte" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="tactics"}

{include file="%s/templates/entities/fields/_form.tpl"|args:$plugin_root field=$field}

{include file="admin/_foot.tpl"}
