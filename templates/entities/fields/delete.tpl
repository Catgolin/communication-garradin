{include file="admin/_head.tpl" title="Supprimer le champ « %s »"|args:$field.title current="plugin_%s"|args:$plugin.id}

{include file="common/delete_form.tpl"
         legend="Supprimer ce champ ?"
         warning="Êtes-vous sûr·e de vouloir supprimer le champ « %s » et tous les contenus s'y rattachant ?"|args:$field.title
         csrf_key="com_delete_field%s"|args:$field.id}

{include file="admin/_foot.tpl"}
