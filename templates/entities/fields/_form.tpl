{form_errors}

<form method="post" action="{$self_url}" enctype="multipart/form-data">
  <fieldset>
    <legend>Champ de texte</legend>
    <dl>
      {input type="text" name="title" label="Titre" required=1 source=$field}
      {input type="textarea" name="description" label="Description" source=$field}
      {input type="number" name="max_length" label="Longueur maximale du texte" help="Nombre maximum de caractères pour le texte" source=$field}
      {input type="number" name="prefered_length" label="Longueur recommandée pour le texte" source=$field}
    </dl>
    <p class="submit">
      {csrf_field key="edit_field"}
      {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    </p>
  </fieldset>
</form>
