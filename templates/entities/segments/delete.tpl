{include file="admin/_head.tpl" title="Supprimer le segment « %s »"|args:$segment.title current="plugin_%s"|args:$plugin.id}

{include file="common/delete_form.tpl"
         legend="Supprimer ce segment ?"
         warning="Êtes-vous sûr·e de vouloir supprimer le segment « %s » ?"|args:$segment.title
         csrf_key="com_delete_segment%s"|args:$segment.id}

{include file="admin/_foot.tpl"}
