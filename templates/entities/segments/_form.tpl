{form_errors}

<form method="post" action="{$self_url}" enctype="multipart/form-data">
  <fieldset>
    <legend>Définition du segment</legend>
    <dl>
      {input type="text" name="title" label="Titre" required=1 source=$segment}
      {input type="textarea" name="description" label="Description" source=$segment}
    </dl>
    <p class="submit">
      {csrf_field key="edit_segment"}
      {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    </p>
  </fieldset>
</form>
