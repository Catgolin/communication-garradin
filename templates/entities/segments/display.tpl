{include file="admin/_head.tpl" title='Segment : %s'|args:$segment.title current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="strategy"}

<dl class="describe">
  <dt>
    {linkbutton href="%ssegments/edit.php?id=%s"|args:$plugin_url,$segment.id shape="edit" label="Éditer"}
    {linkbutton href="%ssegments/delete.php?id=%s"|args:$plugin_url,$segment.id shape="delete" label="Supprimer"}
  </dt>
  <dd>
    <h2>{$segment.title}</h2>
  </dd>
  {if $segment.description}
  <dt>Description</dt>
  <dd>{$segment.description}</dd>
  {/if}
</dl>

{include file="admin/_foot.tpl"}
