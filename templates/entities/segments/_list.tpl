{if !empty($list)}
<table class="list">
  <thead>
    <tr>
      <th class="num">N°</th>
      <th>Titre</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$list item="segment"}
    <tr>
      <td class="num"><a href="{plugin_url file="segments/display.php?id=%s"|args:$segment.id}">{$segment.id}</a></td>
      <td>{$segment.title}</td>
      <td>
        {linkbutton href="%ssegments/edit.php?id=%s"|args:$plugin_url,$segment.id shape="edit" label="Éditer"}
        {linkbutton href="%ssegments/delete.php?id=%s"|args:$plugin_url,$segment.id shape="delete" label="Supprimer"}
      </td>
    </tr>
    {/foreach}
  </tbody>
  <tfoot>
    <tr>
      <td colspan="3" class="actions">
        {linkbutton href="%ssegments/edit.php"|args:$plugin_url shape="plus" label="Définir un nouveau segment"}
      </td>
  </tfoot>
</table>
{/if}
