{include file="admin/_head.tpl" title="Définir un segment" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="strategy"}

{include file="%s/templates/entities/segments/_form.tpl"|args:$plugin_root segment=$segment}

{include file="admin/_foot.tpl"}
