{form_errors}

<form method="post" action="{$self_url}" enctype="multipart/form-data">
  <fieldset>
    <legend>Support ou canal de communication</legend>
    <dl>
      {input type="text" name="title" label="Titre" required=1 source=$support}
      {input type="textarea" name="description" label="Description" source=$support}
      {input type="url" name="url" label="Lien" source=$support}
      {input type="number" name="images_width" label="Largeur des images sur ce support" help="Largeur des images (en pixels)" source=$support}
      {input type="number" name="images_height" label="Hauteur des images sur ce support" help="Hauteur des images (en pixels)" source=$support}
    </dl>
    <p class="submit">
      {csrf_field key="edit_support"}
      {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    </p>
  </fieldset>
</form>
