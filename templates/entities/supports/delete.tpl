{include file="admin/_head.tpl" title="Supprimer le support « %s »"|args:$support.title current="plugin_%s"|args:$plugin.id}

{include file="common/delete_form.tpl"
         legend="Supprimer ce support ?"
         warning="Êtes-vous sûr·e de vouloir supprimer le cannal « %s » et tous les champs s'y rattachant ?"|args:$support.title
         csrf_key="com_delete_support%s"|args:$support.id}

{include file="admin/_foot.tpl"}
