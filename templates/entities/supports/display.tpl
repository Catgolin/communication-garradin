{include file="admin/_head.tpl" title=$support.title current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="strategy"}

<dl class="describe">
  <dt>
    {linkbutton href="%ssupports/edit.php?id=%s"|args:$plugin_url,$support.id shape="edit" label="Éditer"}
    {linkbutton href="%ssupports/delete.php?id=%s"|args:$plugin_url,$support.id shape="delete" label="Supprimer"}
  </dt>
  <dd>
    <h2>{$support.title}</h2>
  </dd>
  {if $support.description }
  <dt>Description</dt>
  <dd>{$support.description}</dd>
  {/if}
  {if $support.url}
  <dt>Lien</dt>
  <dd><a href="{$support.url}">{$support.url}</a></dd>
  {/if}
</dl>

{if $support.images_width || $support.images_height}
<dl class="describe">
  <dt><h3>Images</h3></dt>
  <dd>Taille des images publiées sur ce support</dd>
  {if $support.images_width && $support.images_height}
  <dt>Taille des images</dt>
  <dd>{$support.images_width}x{$support.images_height}px</dd>
  {elseif $support.images_width}
  <dt>Largeur des images</dt>
  <dd>{$support.images_width} pixels</dd>
  {else}
  <dt>Hauteur des images</dt>
  <dd>{$support.images_height} pixels</dd>
  {/if}
</dl>
{/if}

<dl class="describe">
  <dt><h3>Champs pour les publications</h3></dt>
  <dd>Textes permettant d'accompagner les publications sur ce support</dd>
  <dt>
    {linkbutton href="%sfields/edit.php?support=%s"|args:$plugin_url,$support.id shape="plus" label="Ajouter un champ"}
  </dt>
  <dd>
    {include file="%s/templates/entities/fields/_list.tpl"|args:$plugin_root list=$support->getFields()}
  </dd>
</dl>

{include file="admin/_foot.tpl"}
