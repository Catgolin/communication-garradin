{include file="admin/_head.tpl" title="Plan de communication" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root current="publications"}

{linkbutton href="%ssupports/edit.php"|args:$plugin_url shape="plus" label="Ajouter un nouveau support"}

{if count($supports) > 0}
<h2>Supports</h2>

<table class="list">
  <thead>
    <tr>
      <th>Titre</th>
      <th>Lien</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$supports item="support"}
    <tr>
      <td>
        <a href="{"%ssupports/display.php?id=%s"|args:$plugin_url,$support.id}">
          {$support.title}
        </a>
      </td>
      <td><a href="{$support.url}">{$support.url}</a></td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

{include file="admin/_foot.tpl"}
