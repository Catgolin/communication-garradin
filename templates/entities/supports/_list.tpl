{if !empty($list)}
<dl class="describe">
  <dt><h2>Supports de communication</h2></dt>
  <dd>
    {linkbutton href="%ssupports/edit.php"|args:$plugin_url shape="plus" label="Ajouter un support"}
  </dd>
  {foreach from=$list item="support"}
  <dt>{$support.title}</dt>
  <dd>{$support.description}</dd>
  <dd>
    {linkbutton href="%ssupports/display.php?id=%s"|args:$plugin_url,$support.id shape="search" label="Voir"}
  </dd>
  {/foreach}
</dl>
{/if}
