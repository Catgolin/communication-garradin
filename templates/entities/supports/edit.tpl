{include file="admin/_head.tpl" title="Définir un support de communication" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="strategy"}

{include file="%s/templates/entities/supports/_form.tpl"|args:$plugin_root support=$support}

{include file="admin/_foot.tpl"}
