{form_errors}

<form method="post" action="{$self_url}" enctype="multipart/form-data">
  <fieldset>
    <legend>Définition de la campagne</legend>
    <dl>
      {input type="text" name="title" label="Titre" required=1 source=$campaign}
      {input type="textarea" name="description" label="Description" source=$campaign}
      {input type="select" name="id_product" label="Produit à mettre en avant" options=$products default=$campaign.id_product}
      {input type="select" name="id_primary_target" label="Cible principale" options=$segments default=$campaign.id_primary_target}
      {input type="date" name="start" label="Début de l'objectif" source=$campaign}
      {input type="date" name="end" label="Fin de l'objectif" source=$campaign}
    </dl>
    <p class="submit">
      {csrf_field key="edit_campaign"}
      {button type="submit" name="save" label="Enregistrer" shape="right" class="main"}
    </p>
  </fieldset>
</form>
