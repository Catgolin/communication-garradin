{include file="admin/_head.tpl" title="Plan de communication" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root current="objectives"}

{linkbutton href="%sobjectives/edit.php"|args:$plugin_url shape="plus" label="Ajouter un nouvel objectif"}
{linkbutton href="%ssegments/edit.php"|args:$plugin_url shape="plus" label="Définir un nouveau segment"}

{if count($objectives) > 0}
<h2>Objectifs</h2>


<table class="list">
  <thead>
    <tr>
      <th>Titre</th>
      <th>Début</th>
      <th>Fin</th>
      <th>Indicateur</th>
      <th>Avancement</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$objectives item="obj"}
    <?php $n = count($indicators[$obj->id])+1; ?>
    <tr>
      <td{if $n > 1} rowspan="{$n}"{/if}>
        <a href="{"%sobjectives/display.php?id=%s"|args:$plugin_url,$obj.id}">
          {$obj.title}
        </a>
      </td>
      <td{if $n > 1} rowspan="{$n}"{/if}>{$obj.start|date:'d/m/Y'}</td>
      <td{if $n > 1} rowspan="{$n}"{/if}>{$obj.end|date:'d/m/Y'}</td>
      {foreach from=$indicators[$obj->id] item="indicator" key="i"}
        <td>{$indicator.title}</td>
        <td>{$indicator.value}/{$indicator.target} {$indicator.measure}</td>
    </tr>
    <tr>
      {/foreach}
      <td colspan="2">{linkbutton href="%sindicators/edit.php?obj=%s"|args:$plugin_url,$obj.id shape="plus" label="Ajouter un indicateur"}</td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

{include file="admin/_foot.tpl"}
