{include file="admin/_head.tpl" title="Supprimer l'objectif « %s »"|args:$objective.title current="plugin_%s"|args:$plugin.id}

{include file="common/delete_form.tpl"
         legend="Supprimer cet objectif ?"
         warning="Êtes-vous sûr·e de vouloir supprimer l'objectif « %s » et tous les indicateurs s'y rattachant ?"|args:$objective.title
         csrf_key="com_delete_objective%s"|args:$objective.id}

{include file="admin/_foot.tpl"}
