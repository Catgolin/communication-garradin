{include file="admin/_head.tpl" title="Fixer un objectif" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root current="objectives"}

{include file="%s/templates/objectives/_form.tpl"|args:$plugin_root objective=$objective}
