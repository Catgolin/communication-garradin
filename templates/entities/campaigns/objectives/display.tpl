{include file="admin/_head.tpl" title='Objectif : %s'|args:$objective.title current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root current="objectives"}

<dl class="describe">
  <dt>Intitulé de l'objectif</dt>
  <dd><h2>{$objective.title}</h2></dd>
  {if $objective.description}
  <dt>Description</dt>
  <dd>{$objective.description}</dd>
  {/if}
  {if $objective.start}
  <dt>Début de l'objectif</dt>
  <dd>{$objective.start|date:'l j F Y (d/m/Y)'}</dd>
  {/if}
  {if $objective.end}
  <dt>Fin de l'objectif</dt>
  <dd>{$objective.end|date:'l j F Y (d/m/Y)'}</dd>
  {/if}
</dl>
{linkbutton href="%sobjectives/edit.php?id=%s"|args:$plugin_url,$objective.id shape="edit" label="Éditer"}
{linkbutton href="%sobjectives/delete.php?id=%s"|args:$plugin_url,$objective.id shape="delete" label="Supprimer cet objectif"}

{if !empty($indicators)}
<h2>Indicateurs associés à cet objectif</h2>

<table class="list">
  <thead>
    <tr>
      <th>Intitulé</th>
      <th>Date limite</th>
      <th>Avancement</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$indicators item="indicator"}
    <tr>
      <td>{$indicator.title}</td>
      <td>{$indicator.deadline|date:'l j F Y (d/m/Y)'}</td>
      <td>{$indicator.value}/{$indicator.target} {$indicator.metric}</td>
      <td>
        {linkbutton href="%sindicators/edit.php?obj=%s&id=%s"|args:$plugin_url,$objective.id,$indicator.id shape="edit" label="Modifier"}
        <br/>
        {linkbutton href="%sindicators/delete.php?id=%s"|args:$plugin_url,$indicator.id shape="delete" label="Supprimer"}
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

<aside>
  {linkbutton href="%sindicators/edit.php?obj=%s"|args:$plugin_url,$objective.id shape="plus" label="Ajouter un nouvel indicateur"}
</aside>

{include file="admin/_foot.tpl"}
