{include file="admin/_head.tpl" title="Supprimer l'objectif « %s »"|args:$campaign.title current="plugin_%s"|args:$plugin.id}

{include file="common/delete_form.tpl"
         legend="Supprimer cette campagne ?"
         warning="Êtes-vous sûr·e de vouloir supprimer la campagne « %s » ?"|args:$campaign.title
         csrf_key="com_delete_campaign%s"|args:$campaign.id}

{include file="admin/_foot.tpl"}
