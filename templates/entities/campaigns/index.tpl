{include file="admin/_head.tpl" title="Campagnes en cours" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root current="campaigns"}

{linkbutton href="%scampaigns/edit.php"|args:$plugin_url shape="plus" label="Créer une nouvelle campagne"}

{if count($campaigns) > 0}
<h2>Campagnes</h2>

<table class="list">
  <thead>
    <tr>
      <th>Titre</th>
      <th>Cible principale</th>
      <th>Produit</th>
      <th>Début</th>
      <th>Fin</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$campaigns item="camp" key="i"}
    <tr>
      <td>
        <a href="{"%scampaigns/display.php?id=%s"|args:$plugin_url,$camp.id}">
          {$camp.title}
        </a>
      </td>
      <td>
        <a href="{"%ssegments/display.php?id=%s"|args:$plugin_url,$camp.id_primary_target}">
          {$targets[$i]}
        </a>
      </td>
      <td>
        <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$camp.id_product}">
          {$products[$i]}
        </a>
      </td>
      <td>{$camp.start|date:'d/m/Y'}</td>
      <td>{$camp.end|date:'d/m/Y'}</td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

{include file="admin/_foot.tpl"}
