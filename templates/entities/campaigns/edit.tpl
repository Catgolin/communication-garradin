{include file="admin/_head.tpl" title="Définission de la campagne" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="tactics"}

{include file="%s/templates/entities/campaigns/_form.tpl"|args:$plugin_root campaign=$campaign}

{include file="admin/_foot.tpl"}
