{if !empty($list)}
<table class="list">
  <thead>
    <tr>
      <th class="num">N°</th>
      <th>Libellé</th>
      <th>Produit</th>
      <th>Cible principale</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    {foreach from=$list item="campaign"}
    <tr>
      <td class="num"><a href="{plugin_url file="campaigns/display.php?id=%s"|args:$campaign.id}">{$campaign.id}</a></td>
      <td>
        <a href="{plugin_url file="campaigns/display.php?id=%s"|args:$campaign.id}">
          {$campaign.title}
        </a>
      </td>
      <td>
        <a href="{plugin_url file="products/display.php?id=%s"|args:$campaign.id_product}">
          {$campaign->getProduct()->title}
        </a>
      </td>
      <td>
        <a href="{plugin_url file="segments/display.php?id=%s"|args:$campaign.id_primary_target}">
          {$campaign->getPrimaryTarget()->title}
        </a>
      </td>
      <td>
        {linkbutton href="%scampaigns/edit.php?id=%s"|args:$plugin_url,$campaign.id shape="edit" label="Éditer"}
        {linkbutton href="%scampaigns/delete.php?id=%s"|args:$plugin_url,$campaigns.id shape="delete" label="Supprimer"}
      </td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}
