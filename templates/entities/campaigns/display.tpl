{include file="admin/_head.tpl" title='Campagne : %s'|args:$campaign.title current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="tactics"}

<dl class="describe">
  <dt>
    {linkbutton href="%scampaigns/edit.php?id=%s"|args:$plugin_url,$campaign.id shape="edit" label="Éditer"}
    {linkbutton href="%scampaigns/delete.php?id=%s"|args:$plugin_url,$campaign.id shape="delete" label="Supprimer"}
  </dt>
  <dd><h2>{$campaign.title}</h2></dd>
  {if $campaign.description}
  <dt>Description</dt>
  <dd>{$campaign.description}</dd>
  {/if}

  <dt>Public cible</dt>
  <dd>
    <a href="{"%ssegments/display.php?id=%s"|args:$plugin_url,$primary_target.id}">
      {$primary_target.title}
    </a>
  </dd>
  {if $primary_target.description}
  <dd class="help">{$primary_target.description}</dd>
  {/if}

  <dt>Produit</dt>
  <dd>
    <a href="{"%sproducts/display.php?id=%s"|args:$plugin_url,$product.id}">
      {$product.title}
    </a>
  </dd>
  {if $product.description}
  <dd class="help">{$product.description}</dd>
  {/if}

  {if $campaign.start}
  <dt>Début de la campagne</dt>
  <dd>{$campaign.start|date:'l j F Y (d/m/Y)'}</dd>
  {/if}
  {if $campaign.end}
  <dt>Fin de la campagne</dt>
  <dd>{$campaign.end|date:'l j F Y (d/m/Y)'}</dd>
  {/if}
</dl>

<dl class="describe">
  <dt><h3>Formats de publications</h3></dt>
  <dd>Formats utilisés pour les publications de cette campagne</dd>
  <dt>
    {linkbutton href="%stemplates/edit.php?campaign=%s"|args:$plugin_url,$campaign.id shape="plus" label="Ajouter un template"}
  </dt>
  <dd>
    {include file="%s/templates/entities/templates/_list.tpl"|args:$plugin_root list=$templates campaign=$campaign}
  </dd>
</dl>

<dl class="describe">
  <dt><h3>Publications</h3></dt>
  <dd>Éléments prévus ou publiés dans le cadre de la campagne.</dd>
</dl>
{include file="%s/templates/entities/publications/_list.tpl"|args:$plugin_root list=$publications}

{include file="admin/_foot.tpl"}
