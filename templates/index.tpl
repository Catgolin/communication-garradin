{include file="admin/_head.tpl" title="Politique de communication" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="strategy" current="index"}

{if count($supports) == 0 || count($segments) == 0}
<div class="alert block">
  Aucun support ou aucun segment n'est défini.
</div>
<details>
  <summary class="help block">
    Pour commencer à définir la politique de communication de l'association,
    vous pouvez :
    <dl class="describe">
      <dt>Définir les supports de communication de l'association</dt>
      <dd>
        {linkbutton href="%ssupports/edit.php"|args:$plugin_url label="Ajouter un support" shape="plus"}
      </dd>
      <dt>Définir les publics ciblés par la communication</dt>
      <dd>
        {linkbutton href="%ssegments/edit.php"|args:$plugin_url label="Ajouter un segment" shape="plus"}
      </dd>
    </dl>
  </summary>
  <div class="help block">
    <dl class="describe">
      <dt>Support de communication</dt>
      <dd>
        Le support définit les différents endroits où l'association peut diffuser ses messages:
        profils sur les réseaux sociaux, stories, espaces d'affichages, etc.
      </dd>
      <dt>Segment de communication</dt>
      <dd>
        Un segment est une catégorie de la population qui est susceptible d'être touché par une campagne de communication.
      </dd>
      <dd class="help">
        Il vaut mieux définir des segments qui soient :
        <ul>
          <li>homogènes (individus aux comportements proches),</li>
          <li>accessibles (individus pouvant être atteints par des actions spécifiques),</li>
          <li>pertinents (segments susceptibles de générer des idées d'actions objectives, mesurables et efficaces).</li>
        </ul>
        <a href="https://fr.wikipedia.org/wiki/Segmentation_(sciences_humaines)">
          Voir sur Wikipédia
        </a>
      </dd>
  </div>
</details>
{elseif count($objectives) == 0}
<div class="alert block">
  Aucun objectif n'est défini.
  {linkbutton href="%sobjectives/edit.php"|args:$plugin_url shape="plus" label="Définir un objectif"}
</div>
{/if}

{if count($supports) > 0}
<aside class="describe">
  {include file="%s/templates/entities/supports/_list.tpl"|args:$plugin_root list=$supports}
</aside>
{/if}

{if count($segments) > 0}
<dl class="describe">
  <dt><h2>Segments</h2></dt>
  <dd>
    {include file="%s/templates/entities/segments/_list.tpl"|args:$plugin_root list=$segments}
  </dd>
</dl>
{/if}


{if count($objectives) > 0}
<h2>Suivi des objectifs de communication</h2>
{include file="%s/templates/entities/objectives/_list.tpl"|args:$plugin_root list=$objectives}
{/if}

{include file="admin/_foot.tpl"}
