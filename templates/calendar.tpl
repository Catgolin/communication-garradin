{include file="admin/_head.tpl" title="Campagnes de communication" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="operation" current="index"}

{include file="%s/templates/entities/publications/_list.tpl"|args:$plugin_root list=$publications}

{include file="admin/_foot.tpl"}