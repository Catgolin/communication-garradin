{include file="admin/_head.tpl" title="Campagnes de communication" current="plugin_%s"|args:$plugin.id}

{include file="%s/templates/_nav.tpl"|args:$plugin_root level="tactics" current="index"}

{if empty($products)}
<details>
  <summary class="alert block">
    <h2>Aucun produit n'est défini</h2>
    {linkbutton href="%sproducts/edit.php"|args:$plugin_url label="Définir un produit" shape="plus"}
  </summary>
  <div class="help block">
    Les produits correspondent au message porté dans le cadre de la communication.
    Il peut s'agir d'évènements, de services proposés par l'association, d'une actualité ou autre.
  </div>
</details>
{else}

{if !empty($campaigns)}
<dl class="describe">
  <dt>
    {linkbutton href="%scampaigns/edit.php"|args:$plugin_url shape="plus" label="Nouvelle campagne"}
  </dt>
  <dd><h2>Campagnes en cours</h2></dd>
</dl>
{include file="%s/templates/entities/campaigns/_list.tpl"|args:$plugin_root list=$campaigns}
{/if}

<dl class="describe">
  <dt>
    {linkbutton href="%sproducts/edit.php"|args:$plugin_url shape="plus" label="Nouveau produit"}
  </dt>
  <dd><h2>Produits à promouvoir</h2></dt>
</dl>
{include file="%s/templates/entities/products/_list.tpl"|args:$plugin_root list=$products}

{if empty($campaigns)}
<div class="alert block">
  <h3>Aucune campagne en cours</h3>
  {linkbutton href="%scampaigns/edit.php"|args:$plugin_url shape="plus" label="Nouvelle campagne"}
</div>
{/if}
{/if}

{include file="admin/_foot.tpl"}
