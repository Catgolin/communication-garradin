<?php

namespace Garradin;

$db->begin();

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_support
          (
              id INTEGER NOT NULL PRIMARY KEY,
              title TEXT NOT NULL,
              description TEXT,
              images_width INTEGER,
              images_height INTEGER,
              url TEXT
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_segment
          (
              id INTEGER NOT NULL PRIMARY KEY,
              title TEXT NOT NULL,
              description TEXT
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_product
          (
              id INTEGER NOT NULL PRIMARY KEY,
              title TEXT NOT NULL,
              description TEXT,
              action TEXT,
              url TEXT,
              deadline TEXT,
              type INTEGER,
              date_event TEXT,
              time_start TEXT,
              time_end TEXT,
              start TEXT,
              end TEXT,
              id_service INTEGER REFERENCES services(id) ON DELETE SET NULL,
              id_related INTEGER REFERENCES com_product(id) ON DELETE SET NULL
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_objective
          (
              id INTEGER NOT NULL PRIMARY KEY,
              title TEXT NOT NULL,
              description TEXT,
              start TEXT,
              end TEXT
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_indicator
          (
              id INTEGER NOT NULL PRIMARY KEY,
              title TEXT NOT NULL,
              id_objective INTEGER NOT NULL REFERENCES com_objective(id) ON DELETE CASCADE,
              description TEXT,
              target TEXT,
              value TEXT,
              metric TEXT,
              url TEXT,
              deadline TEXT
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_campaign
          (
              id INTEGER NOT NULL PRIMARY KEY,
              title TEXT NOT NULL,
              id_product INTEGER REFERENCES com_product(id) ON DELETE SET NULL,
              id_primary_target INTEGER REFERENCES com_segment(id) ON DELETE SET NULL,
              description TEXT,
              start TEXT,
              end TEXT
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_template
          (
              id INTEGER NOT NULL PRIMARY KEY,
              title TEXT NOT NULL,
              description TEXT,
              id_campaign INTEGER REFERENCES com_campaign(id) ON DELETE SET NULL
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_field
          (
               id INTEGER NOT NULL PRIMARY KEY,
               title TEXT NOT NULL,
               description TEXT,
               max_length INTEGER,
               prefered_length INTEGER,
               id_template INTEGER REFERENCES com_template(id) ON DELETE CASCADE,
               id_support INTEGER REFERENCES com_support(id) ON DELETE CASCADE
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_publication
          (
              id INTEGER NOT NULL PRIMARY KEY,
              title TEXT NOT NULL,
              id_product INTEGER REFERENCES com_product(id) ON DELETE CASCADE,
              id_template INTEGER NOT NULL REFERENCES com_template(id) ON DELETE CASCADE,
              id_campaign INTEGER NOT NULL REFERENCES com_campaign(id) ON DELETE CASCADE,
              release_date TEXT,
              release_time TEXT
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_text
          (
              id INTEGER NOT NULL PRIMARY KEY,
              content TEXT,
              id_field INTEGER NOT NULL REFERENCES com_field(id) ON DELETE CASCADE,
              id_publication INTEGER NOT NULL REFERENCES com_publication(id) ON DELETE CASCADE,
              is_done INTEGER
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_campaign_objectives
          (
              id INTEGER NOT NULL PRIMARY KEY,
              id_campaign INTEGER NOT NULL REFERENCES com_campaign(id) ON DELETE CASCADE,
              id_objective INTEGER NOT NULL REFERENCES com_objective(id) ON DELETE CASCADE
          );
          EOF
);

$db->exec(<<<EOF
          CREATE TABLE IF NOT EXISTS com_template_supports
          (
              id INTEGER NOT NULL PRIMARY KEY,
              id_support INTEGER NOT NULL REFERENCES com_support(id) ON DELETE CASCADE,
              id_template INTEGER NOT NULL REFERENCES com_template(id) ON DELETE CASCADE
          );
          EOF
);

$db->commit();

$plugin->registerSignal('menu.item', 'Garradin\Plugin\Communication\Signaux::menu');
